<?php
    require_once("model.php");
    require_once("util.php");
    
    session_start();

    limpiar_entradas();    

    include("_header.html");
    include("_navbar.html");

    if(isset($_POST["impostor"])) {
        set_impostor($_POST["impostor"]);
        echo '<div class="card-panel blue-text text-darken-2"><i class="material-icons">info</i>Se ha señalado un nuevo impostor</div>';
    }

    include("_form_traidor.html"); 
    include("_footer.html"); 
?>