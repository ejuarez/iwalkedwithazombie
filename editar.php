<?php
    require_once("model.php");
    require_once("util.php");
    
    session_start();

    limpiar_entradas();    

    $nombre = get_nombre($_GET["id"]);
    
    include("_header.html");
    include("_navbar.html");
    
    include("_form_editar.html");

    include("_footer.html"); 
?>