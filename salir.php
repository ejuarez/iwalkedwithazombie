<?php
    require_once("util.php");
    
    session_start();
    session_unset();
    session_destroy();

    include("_header.html");
    include("_navbar.html");
    include("_nosvemos.html");
    include("_footer.html"); 
?>