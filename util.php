<?php

function crear_tabla() {
    $tabla = "<table>";
    
    for($i = 1; $i <= 10; $i++) {
        $tabla .= "<tr>";
        $tabla .= "<td>Zombie</td><td>$i</td>";
        $tabla .= "</tr>";
    }
    
    $tabla .= "</table>";
    return $tabla;
}

function hola($nombre="mundo") {
    return "<br>hola $nombre!";
}

function descubrir_impostor($candidato) {
    $resultado = "";
    $impostor = $_SESSION["impostor"];
    
    if ($candidato == $impostor) {
        $resultado = "¡Descubriste al impostor! Es: $impostor";
    } else {
        $resultado = "No, el impostor no es $candidato. Es más bien alguien con fondo amarillo...<br>";
    }
    
    return $resultado;
}

function acusar_impostor() {
        
    $resultado = "";
    
    if (isset($_GET["impostor"])) {
        $resultado = descubrir_impostor($_GET["impostor"]);
        
    } else if (isset($_POST["impostor"])) {
        $resultado = descubrir_impostor($_POST["impostor"]);
    
    } else {
        $resultado = "Aquí no hay nadie buscando impostores, confiamos en toda la tripulación.";
    }
    
    return $resultado;
}

function limpiar_entradas() {
    if (isset($_GET["impostor"])) {
        $_GET["impostor"] = htmlspecialchars($_GET["impostor"]);
    }

    if (isset( $_POST["impostor"])) {
        $_POST["impostor"] = htmlspecialchars($_POST["impostor"]);
    }
    
    if (isset( $_POST["acusador"])) {
        $_POST["acusador"] = htmlspecialchars($_POST["acusador"]);
    }
    
    if (isset( $_POST["acusado"])) {
        $_POST["acusado"] = htmlspecialchars($_POST["acusado"]);
    }
    
    if (isset( $_POST["id"])) {
        $_POST["id"] = htmlspecialchars($_POST["id"]);
    }
    
    if (isset( $_POST["nombre"])) {
        $_POST["nombre"] = htmlspecialchars($_POST["nombre"]);
    }
    
    if (isset( $_GET["id"])) {
        $_GET["id"] = htmlspecialchars($_GET["id"]);
    }
}

?>