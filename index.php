<?php
    require_once("model.php");
    require_once("util.php");
    
    session_start();

    limpiar_entradas();    

    $respuesta = "";
    $BD = 1;

    if ($BD) {
        if (isset($_POST["acusador"]) && isset($_POST["acusado"])) {
            acusa($_POST["acusador"], $_POST["acusado"]);
            
            $respuesta = verificar_traidor($_POST["acusado"]);
        }
        
    } else {
        if(isset($_POST["impostor"])) {
            $_SESSION["acusacion"] = $_POST["impostor"];
        }

        if(!isset($_SESSION["acusacion"])) {
            $_SESSION["acusacion"] = "No hay culpables entre nosotros";
        }

        $respuesta = acusar_impostor();
    
    }
    
    include("_header.html");
    include("_navbar.html");

    include("_espacio_ajax.html");

    include("_tabla_acusaciones.html");
    include("_impostor.html");
    include("_form.html"); 
    include("_footer.html"); 
?>